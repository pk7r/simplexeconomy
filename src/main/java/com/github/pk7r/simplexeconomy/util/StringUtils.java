package com.github.pk7r.simplexeconomy.util;

import java.math.BigDecimal;
import java.text.NumberFormat;

public class StringUtils {

    private static final NumberFormat FORMAT = NumberFormat.getCurrencyInstance();

    public static String moneyFormat(double money) {
        BigDecimal value = new BigDecimal(money);
        return FORMAT.format(value);
    }
}