package com.github.pk7r.simplexeconomy.util;

import com.github.pk7r.simplexeconomy.Main;
import me.clip.placeholderapi.expansion.PlaceholderExpansion;
import org.bukkit.entity.Player;
import org.jetbrains.annotations.NotNull;

import java.util.Objects;

public class PlaceholderTag extends PlaceholderExpansion {

    private final Main plugin;

    public PlaceholderTag(final Main plugin) {
        this.plugin = plugin;
    }

    public boolean persist() {
        return true;
    }

    public boolean canRegister() {
        return true;
    }

    public @NotNull String getAuthor() {
        return plugin.getDescription().getAuthors().toString();
    }

    public @NotNull String getIdentifier() {
        return "magnata";
    }

    public @NotNull String getVersion() {
        return plugin.getDescription().getVersion();
    }

    public String onPlaceholderRequest(Player player, @NotNull String identifier) {
        if (player == null) {
            return "";
        }
        if (identifier.equals("tag") && Objects.equals(plugin.getConfig().getString("Magnata"),
                player.getName().toLowerCase())) {
            return "§a[Magnata]";
        }
        return "";
    }
}
