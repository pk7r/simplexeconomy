package com.github.pk7r.simplexeconomy.listener;

import com.github.pk7r.simplexeconomy.Main;
import org.bukkit.Bukkit;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;

public class CreateAccountListener implements Listener {

    @EventHandler
    public void join(PlayerJoinEvent e) {
        Bukkit.getScheduler().runTaskAsynchronously(Main.getMain(),() ->
                Main.getEconomy().createPlayerAccount(e.getPlayer()));
    }
}