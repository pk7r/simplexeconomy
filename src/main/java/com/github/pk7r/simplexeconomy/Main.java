package com.github.pk7r.simplexeconomy;

import co.aikar.commands.BukkitCommandManager;
import co.aikar.commands.PaperCommandManager;
import com.github.pk7r.simplexeconomy.command.ChatActionCommand;
import com.github.pk7r.simplexeconomy.command.EconomyCommand;
import com.github.pk7r.simplexeconomy.command.MagnataCommand;
import com.github.pk7r.simplexeconomy.listener.CreateAccountListener;
import com.github.pk7r.simplexeconomy.model.CustomEconomy;
import com.github.pk7r.simplexeconomy.service.EconomyService;
import com.github.pk7r.simplexeconomy.util.PlaceholderTag;
import lombok.Getter;
import lombok.SneakyThrows;
import net.milkbowl.vault.economy.Economy;
import org.bukkit.Bukkit;
import org.bukkit.plugin.ServicePriority;
import org.bukkit.plugin.java.JavaPlugin;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.Statement;
import java.util.Objects;

@SuppressWarnings({"SqlDialectInspection", "SqlNoDataSourceInspection"})
public final class Main extends JavaPlugin {

    @Getter
    private static Connection database;
    @Getter
    private static Main main;
    @Getter
    private static Economy economy;

    @SneakyThrows
    @Override
    public void onLoad() {
        main = this;
        saveDefaultConfig();
        economy = new CustomEconomy();
        Bukkit.getServicesManager().register(Economy.class, economy,
                Objects.requireNonNull(Bukkit.getPluginManager().getPlugin("Vault")), ServicePriority.Highest);
        Statement stmt;
        Class.forName("org.postgresql.Driver");
        database = DriverManager.getConnection(Objects.requireNonNull(
                main.getConfig().getString("database.host")),
                main.getConfig().getString("database.username"),
                main.getConfig().getString("database.password"));
        stmt = database.createStatement();
        stmt.execute("CREATE TABLE IF NOT EXISTS economy ( username varchar(16) NOT NULL," +
                " balance int  NOT NULL,  PRIMARY KEY (username))");
        stmt.close();
    }

    @Override
    public void onEnable() {

        new PlaceholderTag(this).register();
        Bukkit.getPluginManager().registerEvents(new CreateAccountListener(), this);

        BukkitCommandManager manager = new PaperCommandManager(this);
        manager.registerCommand(new EconomyCommand());
        manager.registerCommand(new ChatActionCommand());
        manager.registerCommand(new MagnataCommand());

        Bukkit.getScheduler().scheduleSyncRepeatingTask(this,
                EconomyService::getMoneyTop, 60 * 20, 5 * 60 * 20);
        Bukkit.getScheduler().runTaskTimerAsynchronously(this,
                EconomyService::getMagnata, 60 * 20, 3 * 60 * 20);
    }

    @SneakyThrows
    @Override
    public void onDisable() {
        database.close();
    }

}