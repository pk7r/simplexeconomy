package com.github.pk7r.simplexeconomy.command;

import co.aikar.commands.BaseCommand;
import co.aikar.commands.annotation.*;
import co.aikar.commands.bukkit.contexts.OnlinePlayer;
import com.github.pk7r.simplexeconomy.Main;
import com.github.pk7r.simplexeconomy.util.StringUtils;
import de.themoep.minedown.MineDown;
import net.md_5.bungee.api.chat.BaseComponent;
import org.bukkit.Bukkit;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;

@CommandAlias("money")
@SuppressWarnings("unused")
public class EconomyCommand extends BaseCommand {

    @Default
    @Subcommand("see")
    @CommandCompletion("@players @nothing")
    public void seeMoney(Player player, @Optional String target) {
        if (target == null) {
            String money = StringUtils.moneyFormat(Main.getEconomy().getBalance(player));
            player.spigot().sendMessage(MineDown.parse(String.format("&aSeu money: &e%s"
                    , money)));
            return;
        }
        Player t = Bukkit.getPlayer(target);
        String money = StringUtils.moneyFormat(Main.getEconomy().getBalance(t));
        player.spigot().sendMessage(MineDown.parse(String.format("&aMoney de %s: &e%s", target, money)));
    }

    @Subcommand("give")
    @CommandCompletion("@players @nothing")
    public void giveMoney(CommandSender player, String target, Integer value) {
        if (!player.hasPermission("craftlife.money.give")) {
            player.sendMessage("§cSem permissão.");
            return;
        }
        if (value <= 0) {
            player.sendMessage("§cO valor não pode ser menor que zero.");
            return;
        }
        Player t = Bukkit.getPlayer(target);
        String money = StringUtils.moneyFormat(value);
        Main.getEconomy().depositPlayer(t, value);
        player.spigot().sendMessage(MineDown.parse(String.format("§aA quantia de §e%s§a foi depositada na conta de §e%s&a.", money, target)));
    }

    @Subcommand("take")
    @CommandCompletion("@players @nothing")
    public void takeMoney(Player player, String target, Integer value) {
        if (!player.hasPermission("craftlife.money.take")) {
            player.spigot().sendMessage(MineDown.parse("&cSem permissão."));
            return;
        }
        if (value <= 0) {
            player.spigot().sendMessage(MineDown.parse("&cO valor não pode ser menor que zero."));
            return;
        }
        Player t = Bukkit.getPlayer(target);
        String money = StringUtils.moneyFormat(value);
        Main.getEconomy().withdrawPlayer(t, value);
        player.spigot().sendMessage(MineDown.parse(String.format("&aA quantia de &e%s&a foi retirada na conta de &e%s&a.", money, target)));
    }

    @Subcommand("set")
    @CommandCompletion("@players @nothing")
    public void setMoney(Player player, String target, Integer value) {
        if (!player.hasPermission("craftlife.money.set")) {
            player.spigot().sendMessage(MineDown.parse("&cSem permissão."));
            return;
        }
        if (value < 0) {
            player.spigot().sendMessage(MineDown.parse("&cO valor não pode ser menor que zero."));
            return;
        }
        Player t = Bukkit.getPlayer(target);
        String money = StringUtils.moneyFormat(value);
        double has = Main.getEconomy().getBalance(t);
        Main.getEconomy().withdrawPlayer(t, has);
        Main.getEconomy().depositPlayer(t, value);
        player.spigot().sendMessage(MineDown.parse(String.format("&aA quantia de &e%s&a foi definida na conta de &e%s&a.", money, target)));
    }

    @Subcommand("pay")
    @CommandCompletion("@players @nothing")
    public void payMoney(Player player, OnlinePlayer onlinePlayer, Double value) {
        if (value <= 0) {
            player.spigot().sendMessage(MineDown.parse("&cO valor não pode ser menor que zero."));
            return;
        }
        Player target = onlinePlayer.getPlayer();
        String money = StringUtils.moneyFormat(value);
        if (target.getName().equalsIgnoreCase(player.getName())) {
            player.spigot().sendMessage(MineDown.parse("&cVocê não pode enviar dinheiro para si próprio."));
            return;
        }
        int limit = Main.getMain().getConfig().getInt("limitwithoutalert");
        if (value <= limit) {
            sendMoney(player, value, target, money);
            return;
        }
        String cmd = ChatActionCommand.createTempCommand(() -> {
            if (!Main.getEconomy().has(player, value)) {
                player.spigot().sendMessage(MineDown.parse(String.format("&cVocê não tem %s para enviar para %s.", money, target.getName())));
                return;
            }
            sendMoney(player, value, target, money);
            BaseComponent[] alertMessage = MineDown.parse(String.format("&c%s depositou %s na conta de %s", player.getName(), money, target.getName()));
            Bukkit.getOnlinePlayers().forEach(online -> online.spigot().sendMessage(alertMessage));
        }, 10);

        player.spigot().sendMessage(MineDown.parse(String.format("&aClique abaixo para confimar que quer enviar &e%s&a para &e%s", money, target.getName())));
        player.spigot().sendMessage(MineDown.parse(String.format("[&6[Eu quero enviar %s para %s]](%s hover=&eClique para confirmar)", money, target.getName(), cmd)));
    }

    private void sendMoney(Player player, Double value, Player target, String money) {
        if (Main.getMain().getConfig().getDouble("tax_value") > 0) {
            double taxValue = (value * Main.getMain().getConfig().getDouble("tax_value"));
            double newValue = (value + taxValue);
            if (!Main.getEconomy().has(player, newValue)) {
                if (Main.getEconomy().has(player, value)) {
                    player.spigot().sendMessage(MineDown.parse("&cVocê não tem dinheiro para pagar a tarifa."));
                    return;
                }
                player.spigot().sendMessage(MineDown.parse(String.format("&cVocê não tem %s para enviar para %s.", StringUtils.moneyFormat(newValue), target.getName())));
                return;
            }
            Main.getEconomy().withdrawPlayer(player, newValue);
            String taxString = (Main.getMain().getConfig().getDouble("tax_value") * 100) + "%";
            player.spigot().sendMessage(MineDown.parse(String.format("&e%s &ado valor que você enviou foi descontado da sua conta.", taxString)));
        } else {
            if (!Main.getEconomy().has(player, value)) {
                player.spigot().sendMessage(MineDown.parse(String.format("&cVocê não tem %s para enviar para %s.", money, target.getName())));
                return;
            }
            Main.getEconomy().withdrawPlayer(player, value);
        }
        Main.getEconomy().depositPlayer(target, value);
        player.spigot().sendMessage(MineDown.parse(String.format("&aVocê depositou &e%s&a na conta de &e%s&a.", money, target.getName())));
        target.spigot().sendMessage(MineDown.parse(String.format("&a%s depositou &e%s&a na sua conta.", target.getName(), money)));
    }

    @SuppressWarnings({"SqlDialectInspection", "SqlNoDataSourceInspection"})
    @Subcommand("top")
    @CommandCompletion("@players @nothing")
    public void topMoney(Player player) {
        try {
            Connection db = Main.getDatabase();
            Statement stmt;
            stmt = db.createStatement();
            stmt.execute("SELECT * FROM economy ORDER BY balance DESC limit 5;");
            ResultSet rs = stmt.getResultSet();
            int i = 1;
            player.spigot().sendMessage(MineDown.parse("&2Top 5 Money:"));
            while (rs.next()) {
                player.spigot().sendMessage(MineDown.parse(String.format("&e#%d. &a%s &f- &aR%s", i, rs.getString("username"),
                        StringUtils.moneyFormat(rs.getDouble("balance")
                        ))));
                i++;
            }
            rs.close();
            stmt.close();
        } catch (Exception e) {
            player.spigot().sendMessage(MineDown.parse("&cErro desconhecido."));
        }
    }

    @Subcommand("reload")
    public void reloadCommand(Player player) {
        if (!player.hasPermission("money.reload")) {
            player.spigot().sendMessage(MineDown.parse("&cSem permissão."));
            return;
        }
        Main.getMain().reloadConfig();
        Main.getMain().saveDefaultConfig();
        player.spigot().sendMessage(MineDown.parse("&aConfiguração recarregada com sucesso."));
    }
}