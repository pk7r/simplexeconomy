package com.github.pk7r.simplexeconomy.command;

import co.aikar.commands.BaseCommand;
import co.aikar.commands.annotation.CommandAlias;
import co.aikar.commands.annotation.Default;
import co.aikar.commands.annotation.Description;
import com.github.pk7r.simplexeconomy.Main;
import de.themoep.minedown.MineDown;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

import java.util.HashMap;
import java.util.UUID;

@CommandAlias("chataction")
@SuppressWarnings("unused")
public class ChatActionCommand extends BaseCommand {

    private static final HashMap<UUID, Runnable> actions = new HashMap<>();

    public static String createTempCommand(Runnable r, int expires) {
        final UUID uuid = UUID.randomUUID();
        actions.put(uuid, r);
        if (expires > 0) {
            Bukkit.getScheduler().scheduleSyncDelayedTask(Main.getMain(), () ->
                    actions.remove(uuid), 20L * expires);
        }
        return "/simplexeconomy:chataction " + uuid.toString();
    }

    @Default
    public void chatAction(Player player, String uuids) {
        UUID uuid = null;
        try {
            uuid = UUID.fromString(uuids);
        } catch (Exception ex) {
            return;
        }
        if (!actions.containsKey(uuid)) {
            player.spigot().sendMessage(MineDown.parse("&cthe action has already been used"));
            return;
        }
        Runnable r = actions.remove(uuid);
        r.run();
    }
}