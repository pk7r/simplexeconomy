package com.github.pk7r.simplexeconomy.command;

import co.aikar.commands.BaseCommand;
import co.aikar.commands.annotation.CatchUnknown;
import co.aikar.commands.annotation.CommandAlias;
import co.aikar.commands.annotation.Default;
import com.github.pk7r.simplexeconomy.Main;
import de.themoep.minedown.MineDown;
import org.bukkit.entity.Player;

@CommandAlias("magnata")
public class MagnataCommand extends BaseCommand {

    @Default @CatchUnknown
    public void magnataCommand(Player player) {
        player.spigot().sendMessage(MineDown.parse(String.format("&aO magnata atual é &e%s&a.",
                Main.getMain().getConfig().getString("Magnata"))));
    }
}