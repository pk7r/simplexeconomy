package com.github.pk7r.simplexeconomy.service;

import com.github.pk7r.simplexeconomy.Main;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.HashMap;

@SuppressWarnings("all")
public class EconomyService {

    private static final HashMap<String, Double> moneyTop = new HashMap<>();

    public static void getMoneyTop() {
        try {
            Connection db = Main.getDatabase();
            Statement stmt;
            stmt = db.createStatement();
            stmt.execute("SELECT * FROM economy ORDER BY balance DESC limit 10;");
            ResultSet rs = stmt.getResultSet();
            moneyTop.clear();
            while (rs.next()) {
                moneyTop.put(rs.getString("username"), rs.getDouble("balance"));
            }
            rs.close();
            stmt.close();
        } catch (Exception ignored) {
        }
    }

    public static void getMagnata() {
        try {
            Connection db = Main.getDatabase();
            Statement stmt;
            stmt = db.createStatement();
            stmt.execute("SELECT * FROM economy ORDER BY balance DESC limit 1;");
            ResultSet rs = stmt.getResultSet();
            while (rs.next()) {
                Main.getMain().getConfig().set("Magnata", rs.getString("username"));
                Main.getMain().saveConfig();
            }
            rs.close();
            stmt.close();
        } catch (Exception ignored) {
        }
    }
}